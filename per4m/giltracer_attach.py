# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import argparse
import os
import signal
import sys
import time
from typing import Tuple, Optional

import viztracer
from viztracer.report_builder import ReportBuilder

from per4m.giltracer import PerfRecordSched, PerfRecordGIL

usage = """

Run VizTracer and perf simultaneously, then inject the GIL information in the viztracer output.
This attaches to an existing process which MUST have called VizTracer.install() already.

Usage:

$ giltracer-attach -p 123
"""


# This 'attaches' to a process with VizTracer installed.
# In reality the process under test does all the work and we just send signals
# telling it to do that work.
# It needs to be configured with the right paths beforehand.
#
# Lifted from https://github.com/gaogaotiantian/viztracer/blob/3bd46e7cc3e02de623c8f544abc9cee4a98f45f0/src/viztracer/main.py#L455
# under the Apache licence, with adaptation.
# https://github.com/gaogaotiantian/viztracer/blob/master/NOTICE.txt
def viztracer_attach(pid: int, duration_secs: float) -> None:
    if sys.platform == "win32":
        raise RuntimeError("VizTracer does not support this feature on Windows")

    try:
        os.kill(pid, signal.SIGUSR1)
    except OSError:
        raise RuntimeError(f"pid {pid} does not exist")

    try:
        time.sleep(duration_secs)
    except KeyboardInterrupt:
        pass

    try:
        os.kill(pid, signal.SIGUSR2)
    except OSError:
        raise RuntimeError(f"pid {pid} already finished")


def main(argv=sys.argv):
    parser = argparse.ArgumentParser(argv[0],
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     usage=usage)
    parser.add_argument('--process', '-p', help='The PID of the process to attach to.', required=True)
    parser.add_argument('--duration', '-d', help='How long to attach for, in seconds.', default='30')
    parser.add_argument('--verbose', '-v', action='count', default=1)
    parser.add_argument('--quiet', '-q', action='count', default=0)
    parser.add_argument('--output', '-o', dest="output", default='giltracer.html', help="Output filename (default %(default)s)")
    parser.add_argument('--state-detect', help="Use perf sched events to detect if a process is sleeping due to the GIL (default: %(default)s)", default=False, action='store_true')
    parser.add_argument('--no-state-detect', dest="state_detect", action='store_false')
    parser.add_argument('--gil-detect', help="Use uprobes to detect who has the GIL (read README.md) (default: %(default)s)", default=True, action='store_true')
    parser.add_argument('--no-gil-detect', dest="gil_detect", action='store_false')

    parser.add_argument('args', nargs=argparse.REMAINDER)
    args = parser.parse_args(argv[1:])
    verbose = args.verbose - args.quiet

    pid = int(args.process)
    duration_secs = float(args.duration)

    perf1 = PerfRecordSched(verbose=verbose, pid=pid) if args.state_detect else None
    perf2 = PerfRecordGIL(verbose=verbose, pid=pid, use_pytrace=False) if args.gil_detect else None

    # The viztracer process must already be configured to dump its report to
    # the path that we expect.

    if perf1:
        perf1.start()
    if perf2:
        perf2.start()

    try:
        # This makes the process under test emit VizTracer info for the duration
        # and sleeps for long enough (i.e. it controls the timing).
        viztracer_attach(pid, duration_secs)
    finally:
        if perf1:
            perf1.stop()
        if perf2:
            perf2.stop()

        if perf1:
            perf1.post_process()
        if perf2:
            perf2.post_process()

    input("Wait for viztracer.json to dump, then press enter...")

    files = ['viztracer.json']
    if perf1:
        files.append('schedtracer.json')
    if perf2:
        files.append('giltracer.json')
    builder = ReportBuilder(files, verbose=verbose)
    builder.save(output_file=args.output)


if __name__ == '__main__':
    main()
